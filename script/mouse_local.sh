#!/bin/zsh

# toggle between 5 positions : 1:x 2:↖ 3:↗ 4:↘ 5:↙
motion()
{
	case "$7 $8" in
		"$2 $5") echo "$1 $4";;
		"$1 $4") echo "$3 $4";;
		"$3 $4") echo "$3 $6";;
		"$3 $6") echo "$1 $6";;
		*)       echo "$2 $5";;
	esac
}

xdotool mousemove $(motion $(
xdotool getwindowgeometry $(xdotool getwindowfocus) |
	tr "\n" " " |
	awk -F '[ x,]' '{printf("%d %d %d %d %d %d ",
	$6+5, $6+$13/2, $6+$13-5, $7+5, $7+$14/2, $7+$14-5)
}';

xdotool getmouselocation |
	awk -F '[ :]' '{printf("%d %d\n", $2, $4)
}';
))
